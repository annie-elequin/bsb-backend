var express = require('express');
var router = express.Router();

/* ITEMS PAGE -- /api/items */
router.get('/', function(req, res, next) {
  // res.send('Available Routes: users, items, orders, staff');
  res.json({message: "ITEMS PAGE"});
});

module.exports = router;
